1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.solvd.carinademoapplication"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="21"
8-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="28" />
9-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.INTERNET" />
11-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:4:5-67
11-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:4:22-64
12    <!-- Include required permissions for Google Maps API to run. -->
13    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
13-->[com.google.android.gms:play-services-maps:16.1.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/21ad4dd370ef47f2f2226d4627a4aad2/play-services-maps-16.1.0/AndroidManifest.xml:23:5-79
13-->[com.google.android.gms:play-services-maps:16.1.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/21ad4dd370ef47f2f2226d4627a4aad2/play-services-maps-16.1.0/AndroidManifest.xml:23:22-76
14
15    <uses-feature
15-->[com.google.android.gms:play-services-maps:16.1.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/21ad4dd370ef47f2f2226d4627a4aad2/play-services-maps-16.1.0/AndroidManifest.xml:26:5-28:35
16        android:glEsVersion="0x00020000"
16-->[com.google.android.gms:play-services-maps:16.1.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/21ad4dd370ef47f2f2226d4627a4aad2/play-services-maps-16.1.0/AndroidManifest.xml:27:9-41
17        android:required="true" />
17-->[com.google.android.gms:play-services-maps:16.1.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/21ad4dd370ef47f2f2226d4627a4aad2/play-services-maps-16.1.0/AndroidManifest.xml:28:9-32
18
19    <application
19-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:6:5-30:19
20        android:allowBackup="true"
20-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:7:9-35
21        android:appComponentFactory="android.support.v4.app.CoreComponentFactory"
21-->[com.android.support:support-compat:28.0.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/3e46f6b9930c27683b5284fd6af4a5a5/support-compat-28.0.0/AndroidManifest.xml:22:18-91
22        android:debuggable="true"
23        android:icon="@mipmap/ic_launcher"
23-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:8:9-43
24        android:label="@string/app_name"
24-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:9:9-41
25        android:roundIcon="@mipmap/ic_launcher_round"
25-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:10:9-54
26        android:supportsRtl="true"
26-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:11:9-35
27        android:theme="@style/AppTheme" >
27-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:12:9-40
28        <meta-data
28-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:14:9-16:55
29            android:name="com.google.android.geo.API_KEY"
29-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:15:13-58
30            android:value="@string/google_maps_key" />
30-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:16:13-52
31
32        <activity android:name="com.solvd.carinademoapplication.ActivityTestScreens" />
32-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:18:9-57
32-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:18:19-54
33        <activity android:name="com.solvd.carinademoapplication.ActivityLoginForm" />
33-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:20:9-55
33-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:20:19-52
34        <activity android:name="com.solvd.carinademoapplication.ActivitySplash" >
34-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:22:9-29:20
34-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:22:19-49
35            <intent-filter>
35-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:23:13-28:29
36                <action android:name="android.intent.action.MAIN" />
36-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:24:17-69
36-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:24:25-66
37                <action android:name="android.intent.action.VIEW" />
37-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:25:17-69
37-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:25:25-66
38
39                <category android:name="android.intent.category.LAUNCHER" />
39-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:27:17-77
39-->/var/lib/jenkins/workspace/BuildAPK/app/src/main/AndroidManifest.xml:27:27-74
40            </intent-filter>
41        </activity>
42        <!-- Needs to be explicitly declared on P+ -->
43        <uses-library
43-->[com.google.android.gms:play-services-maps:16.1.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/21ad4dd370ef47f2f2226d4627a4aad2/play-services-maps-16.1.0/AndroidManifest.xml:33:9-35:40
44            android:name="org.apache.http.legacy"
44-->[com.google.android.gms:play-services-maps:16.1.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/21ad4dd370ef47f2f2226d4627a4aad2/play-services-maps-16.1.0/AndroidManifest.xml:34:13-50
45            android:required="false" />
45-->[com.google.android.gms:play-services-maps:16.1.0] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/21ad4dd370ef47f2f2226d4627a4aad2/play-services-maps-16.1.0/AndroidManifest.xml:35:13-37
46
47        <activity
47-->[com.google.android.gms:play-services-base:16.0.1] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/7e305c0ff1d02f64574e93d6d57091d7/play-services-base-16.0.1/AndroidManifest.xml:23:9-26:75
48            android:name="com.google.android.gms.common.api.GoogleApiActivity"
48-->[com.google.android.gms:play-services-base:16.0.1] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/7e305c0ff1d02f64574e93d6d57091d7/play-services-base-16.0.1/AndroidManifest.xml:24:13-79
49            android:exported="false"
49-->[com.google.android.gms:play-services-base:16.0.1] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/7e305c0ff1d02f64574e93d6d57091d7/play-services-base-16.0.1/AndroidManifest.xml:25:13-37
50            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
50-->[com.google.android.gms:play-services-base:16.0.1] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/7e305c0ff1d02f64574e93d6d57091d7/play-services-base-16.0.1/AndroidManifest.xml:26:13-72
51
52        <meta-data
52-->[com.google.android.gms:play-services-basement:16.0.1] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/fc87db0cc6ef073d5a53fc89e768d4ad/play-services-basement-16.0.1/AndroidManifest.xml:23:9-25:69
53            android:name="com.google.android.gms.version"
53-->[com.google.android.gms:play-services-basement:16.0.1] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/fc87db0cc6ef073d5a53fc89e768d4ad/play-services-basement-16.0.1/AndroidManifest.xml:24:13-58
54            android:value="@integer/google_play_services_version" />
54-->[com.google.android.gms:play-services-basement:16.0.1] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/fc87db0cc6ef073d5a53fc89e768d4ad/play-services-basement-16.0.1/AndroidManifest.xml:25:13-66
55
56        <provider
56-->[com.squareup.picasso:picasso:2.71828] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/377180392d4e3af378cda05142ef2eaf/picasso-2.71828/AndroidManifest.xml:8:9-11:40
57            android:name="com.squareup.picasso.PicassoProvider"
57-->[com.squareup.picasso:picasso:2.71828] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/377180392d4e3af378cda05142ef2eaf/picasso-2.71828/AndroidManifest.xml:9:13-64
58            android:authorities="com.solvd.carinademoapplication.com.squareup.picasso"
58-->[com.squareup.picasso:picasso:2.71828] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/377180392d4e3af378cda05142ef2eaf/picasso-2.71828/AndroidManifest.xml:10:13-72
59            android:exported="false" />
59-->[com.squareup.picasso:picasso:2.71828] /var/lib/jenkins/.gradle/caches/transforms-2/files-2.1/377180392d4e3af378cda05142ef2eaf/picasso-2.71828/AndroidManifest.xml:11:13-37
60    </application>
61
62</manifest>
